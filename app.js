
//All models despite FriendModel
var friendDisplay = document.getElementById('friendDisp');
var inputField = document.getElementById('messageInput');
var chatDisplay = document.getElementById('chatDisp');
var statDisplay = document.getElementById('statistics');
var secondStatDisplay = document.getElementById('statisticsTwo');
var typing = document.getElementById('typing');
var addButton = document.getElementById('add');
var changeUserNameButton = document.getElementById('addUser');
var lastClicked = "Zhaslan", currentUser = "Aidana", userName = "Askhat";
var chatHistory = JSON.parse(localStorage.getItem("chat"));
var send = document.getElementById('sendMessage');
var refresh = document.getElementById('refreshMessage');
var search = document.getElementById('searchButton');
var seconds = 0, numberOfChats = 0, totalSMS = 0, totalSearch = 0, totalStorage = 0, clickCount = 0;
var FriendsCollection = Backbone.Collection.extend({
    model: FriendModel,
    initialize: function() {
        console.log("collection created");
    }
});






function UpdateUserName(key, newName) {
    var currentFriends = JSON.parse(localStorage.getItem(key));
    for(var i = 0; i < currentFriends.length; i++) {
        if(currentFriends[i].name == userName) {
            currentFriends[i].name = newName;
            break;
        }
    }
    localStorage.setItem("friends", JSON.stringify(currentFriends));
    console.log("userName updated!");
    for (var i = 0; i < currentFriends.length; i++) {
        console.log(currentFriends[i].name);
    }
}


function UpdateStatus(key, newStatus) {
    var currentFriends = JSON.parse(localStorage.getItem(key));
    for(var i = 0; i < currentFriends.length; i++) {
        if(currentFriends[i].name == userName) {
            currentFriends[i].status = newStatus;
            break;
        }
    }
    localStorage.setItem("friends", JSON.stringify(currentFriends));
    console.log("status updated!");
    for (var i = 0; i < currentFriends.length; i++) {
        console.log(currentFriends[i].name + " " + currentFriends[i].status);
    }
}

function UpdateCollection(cur, key, toAdd) {
    var currentFriends = JSON.parse(localStorage.getItem(key));
    var toReturnCollection = [];
    for(var i = 0; i < currentFriends.length; i++) {
        var curFriend = {
            name: currentFriends[i].name,
            status: currentFriends[i].status,
            lastVisit: currentFriends[i].lastVisit,
            age: currentFriends[i].age
        }
        //console.log(curFriend);
        toReturnCollection.push(curFriend);
    }

    toReturnCollection.push(toAdd);
    localStorage.setItem(key, JSON.stringify(toReturnCollection));
}






window.setInterval(function() {
    //alert("FROM INTERVAL");

    var allFriends = JSON.parse(localStorage.getItem("friends"));
    var allSizes = JSON.parse(localStorage.getItem("messageSizes"));

    for(var i = 0; i < allFriends.length; i++) {
        var currentName = allFriends[i].name;
        if(currentName != userName) {
            //finding allSize with currentName
            for(var j = 0; j < allSizes.length; j++) {
                if(currentName == allSizes[j].name) {
                    var currentKey = currentName + userName;
                    var getChat = JSON.parse(localStorage.getItem(currentKey));
                    //var getAnotherChat = JSON.parse(localStorage.getItem(userName + currentName));
                    if(getChat.length > allSizes[j].size) {
                        alert("MESSAGE RECEIVED FROM " + currentName);
                        allSizes[j].size = getChat.length;
                        localStorage.setItem("messageSizes", JSON.stringify(allSizes));

                        ///here I should display all
                        friendDisplay.innerHTML = "";
                        var nameDiv = "<div class = nameDiv>" + allFriends[i].name + " " + allFriends[i].lastVisit + "</div>";
                        var statusDiv = "<div class = statusDiv>" + allFriends[i].status + "</div>";
                        var templateDiv = "<div class = friendList>" + nameDiv + statusDiv + "</div>";
                        friendDisplay.innerHTML += templateDiv;

                        for(var k = 0; k < allFriends.length; k++) {
                            if(allFriends[k].name != currentName) {
                                var nameDiv = "<div class = nameDiv>" + allFriends[k].name + " " + allFriends[k].lastVisit.substring(0, 8) + "</div>";
                                var statusDiv = "<div class = statusDiv>" + allFriends[k].status + " " + allFriends[k].lastVisit.substring(0, 8) + "</div>";
                                var templateDiv = "<div class = friendList>" + nameDiv + statusDiv + "</div>";
                                friendDisplay.innerHTML += templateDiv;
                            }
                        }


                        var tmp =
                            "<div class='message' contenteditable = 'true'>" +
                            "<img class='avatar' src = 'img/ttt.png' contenteditable = 'true'" + getChat[getChat.length - 1].src +"' alt=''/>" +
                            "<p class='text' contenteditable = 'true'>" + getChat[getChat.length - 1].text + "</p>" + "<div class='clear'></div>" + "</div>"+
                        "<p class='datetime' contenteditable = 'true'>" + getChat[getChat.length - 1].dateTime + " not deleted " + "</p>" + "<div class='clear'></div>";

                        chatDisplay.innerHTML += tmp;
                        lastClicked = currentName;
                        break;

                    }
                }
            }
        }
    }

}, 2000);
window.setInterval(function() {

    document.getElementById('statistics').innerHTML = "";
    var statsMessage = "<div class = 'sms'>" + "Total messages: " +  totalSMS +  "</div>";
    var searchStats = "<div class = 'sms'>" + "Total searches: " +  totalSearch +  "</div>";
    statDisplay.innerHTML += statsMessage;
    secondStatDisplay.innerHTML = searchStats;

    $('.text').emoticonize({ });
    $('.friendList').emoticonize();

    $('.message').mousedown(function(e) {

        if( e.button == 2) {
            $(this).css({
                    'background-color': 'red'
            });
            return false;
        }
        return true;
    });
    $('.message').dblclick(function() {

        $(this).remove();
    });
}, 1000);


$('document').ready(function() {

    userName = prompt("Enter your Name...", "Askhat");
    //userName = "bkbbkb";
    var nam = document.getElementById('userName');
    nam.innerHTML = userName;

    if(!localStorage.getItem("friends")) {
        localStorage.setItem("friends", JSON.stringify([]));
        // name: 'name',
        // description: 'desc',
        // lastVisit: new Date().toLocaleTimeString() + " " + new Date().toDateString(),
        // size: 100
        var Kama = new FriendModel({
            name: "Kamila",
            status: "Меня трудно найти легко потерять :-)",
            lastVisit: "12:02:44" + " " + new Date().toDateString,
            age: 20
        });

        var Aslan = new FriendModel({
            name: "Aslan",
            status: "Жизнь это АСМ, АСМ это жизнь",
            lastVisit: "15:01:43" + " " + new Date().toDateString,
            age: 21
        });
        var Zhaslan = new FriendModel({
            name: "Zhaslan",
            status: "I HAVE A JS FATIGUE :-(",
            lastVisit: "15:02:44" + " " + new Date().toDateString,
            age: 21
        });
        var myFriends = new FriendsCollection();
        myFriends.add(Kama);
        myFriends.add(Aslan);
        myFriends.add(Zhaslan);
        localStorage.setItem("friends", JSON.stringify(myFriends));

    }
    var allFriends = JSON.parse(localStorage.getItem("friends"));



    //if UserName is already there, refresh values
    for(var i = 0; i < allFriends.length; i++) {
        var currentName = allFriends[i].name;
        if(currentName == userName) {
            var currentTime = new Date().toLocaleTimeString() + " " + new Date().toDateString();
            allFriends[i].lastVisit = currentTime;
        }
    }
    //console.log()
    var enteredUser = {
        name: userName,
        status: "available",
        lastVisit: new Date().toLocaleTimeString() + " " + new Date().toDateString,
        age: 20
    };
    //Sorting users
    //var allFriends = JSON.parse(localStorage.getItem("friends"));
    var mapOfFriends = [];
    for(var i = 0; i < allFriends.length; i++) {
        var objectToAdd = {
            //minutes and hours
            minutes: allFriends[i].lastVisit.substring(3, 5),
            hours: parseInt(allFriends[i].lastVisit.substring(0, 2), 10),
            name: allFriends[i].name,
            status: allFriends[i].status,
            lastVisit: allFriends[i].lastVisit.substring(0, 8)
        }
        mapOfFriends.push(objectToAdd);
    }
    for(var i = 0; i < allFriends.length; i++) {
        console.log(mapOfFriends[i].name + " " + mapOfFriends[i].hours + " " + mapOfFriends[i].minutes);
    }
    //nds.push(enteredObjectToAdd);
    mapOfFriends.sort(function(a, b) {
        return b.hours - a.hours;
    });

    for(var i = 0; i < mapOfFriends.length; i++) {
        var curHour = mapOfFriends[i].hours;
        var maxIndex = i, maxMinutes = mapOfFriends[i].minutes;
        for(var j = i + 1; j < mapOfFriends.length; j++) {
            if(mapOfFriends[j].hours != curHour) {
                break;
            }
            if(mapOfFriends[j].minutes >= maxMinutes) {
                maxMinutes = mapOfFriends[j].minutes;
                maxIndex = j;
            }
        }
        var tmp = mapOfFriends[i];
        mapOfFriends[i] = mapOfFriends[maxIndex];
        mapOfFriends[maxIndex] = tmp;
    }


    //Show
    for (var i = 0; i < mapOfFriends.length; i++) {
        if(i == 0) {
            lastClicked = mapOfFriends[i].name;
        }
        var nameDiv = "<div class = nameDiv>" + mapOfFriends[i].name + " " + mapOfFriends[i].lastVisit + "</div>";
        var statusDiv = "<div class = statusDiv>" + mapOfFriends[i].status + "</div>";
        var templateDiv = "<div class = friendList>" + nameDiv + statusDiv + "</div>";
        friendDisplay.innerHTML += templateDiv;
    }

    //Updating and Creating localStorage
    //updating localStorage
    //CHECK BEFORE ADDING
    UpdateCollection(JSON.parse(localStorage.getItem("friends")), "friends", enteredUser);
    alert("You are the " + JSON.parse(localStorage.getItem("friends")).length + "th user:)");
    var curFriends = JSON.parse(localStorage.getItem("friends"));
    var toMessageSizes = [];

    ///Creating MessageSizes localStorage
    for(var k = 0; k < curFriends.length; k++) {
        var toAddObject = {
            name: curFriends[k].name,
            size: 0
        }
        toMessageSizes.push(toAddObject);
    }
    localStorage.setItem("messageSizes", JSON.stringify(toMessageSizes))

    for(var i = 0; i < curFriends.length; i++) {
        var curName = curFriends[i].name;
        for(var j = 0; j < curFriends.length; j++) {
            if(curFriends[j].name != curName) {
                var createKey = curName + curFriends[j].name;
                if(!localStorage.getItem(createKey)) {
                    // /console.log(createKey);
                    localStorage.setItem(createKey, JSON.stringify([]));
                }
            }
        }
    }

    //var send = document.getElementById('sendMessage');
    //addStatus1123
    //var addStatus = document.getElementById('add');
    changeUserNameButton.addEventListener('click', function() {
        if (document.getElementById('addStatus').value == ""){
            alert("Please, Enter something in changeUserNameInput....");
            return;
        }
        var userInput = document.getElementById('addStatus').value;
        alert("Your userName is " + userInput);

        UpdateUserName("friends", userInput);


        alert("UserName has updated!");

    });

    addButton.addEventListener('click', function() {

        if (document.getElementById('addStatus').value == ""){
            alert("Please, Enter something in changeStatusInput....");
            return;
        }
        var statusInput = document.getElementById('addStatus').value;
        alert(statusInput);
        //changing the status

        //UpdateCollection(JSON.parse(localStorage.getItem("friends")), "friends", enteredUser);
        UpdateStatus("friends", statusInput);
        alert("Your status has updated!");


    });

    search.addEventListener('click',function(){
        if (document.getElementById('searchInput').value == ""){
            alert("Please, Enter something in searchInput....");
            return
        }
        var searchInput = document.getElementById('searchInput').value;
        alert(searchInput);
        for(var i = 0; i < curFriends.length; i++) {
            var curName = curFriends[i].name;
            for(var j = 0; j < curFriends.length; j++) {
                if(curFriends[j].name != curName) {
                    var createKey = curName + curFriends[j].name;
                    var curHist = JSON.parse(localStorage.getItem(createKey));
                    //console.log(createKey);
                    for(var k = 0; k < curHist.length; k++) {
                        var curLine = curHist[k].text;
                        //console.log(curLine);
                        if(curLine.includes(searchInput)) {
                            totalSearch++;
                            alert("FOUND!! " + curHist[k].text + " " + curHist[k].dateTime + " road is:" + createKey);
                            break;
                        }
                    }
                }
            }
        }


    });

    $('.friendList').dblclick(function() {
        var clickedElement = $(this).html();
        //alert(clickedElement + " is doubleClicked!!");
        //console.log(clickedElement.substring(4, 10));
        var toIndex = 0;
        for(var i = 0; i < clickedElement.length; i++) {
            if(i >= 4 && clickedElement[i] == '<') {
                toIndex = i;
                break;
            }
        }

        var currentKey = userName + clickedElement.substring(5, toIndex);

        //Updating localStorage chat
        localStorage.setItem(currentKey, JSON.stringify([]));
        var start = 0, end = 0;

        for(var j = 0; j < clickedElement.length; j++) {
            if(clickedElement[j] == '>') {
                start = j + 1;
                break;
            }
        }


        for(var j = 21; j < clickedElement.length; j++) {
            if(!isNaN(clickedElement[j])) {
                end = j;
                break;
            }
        }

        alert(clickedElement.substring(21, end) + " has DELETED!");

        //Removing from localStorage
        localStorage.setItem(userName + clickedElement.substring(21, end), JSON.stringify([]));

        $(this).remove();
    });

    $('.friendList').click(function() {
        var clickedElement = $(this).html();
        var toIndex = 0;
        for(var i = 0; i < clickedElement.length; i++) {
            if(i >= 4 && clickedElement[i] == '<') {
                toIndex = i;
                break;
            }
        }

        var currentKey = userName + clickedElement.substring(5, toIndex);
        var hist = JSON.parse(localStorage.getItem(currentKey));
        console.log(clickedElement.substring(5, toIndex) + " is clicked");
        document.getElementById('chatDisp').innerHTML = "";
        var myKey = userName + clickedElement.substring(5, toIndex);
        //alert(myKey);
        var currentHistory = JSON.parse(localStorage.getItem(myKey));
        //console.log(currentHistory.length);
        if(currentHistory != null) {
            for(var i = 0; i < currentHistory.length; i++) {
                var templateDiv =
                    "<div class='message' contenteditable = 'true'>" +
                    "<img class='avatar' src = 'img/bkb.png' contenteditable = 'true'" + currentHistory[i].src +"' alt=''/>" +
                    "<p class='text' contenteditable = 'true'>" + currentHistory[i].text + "</p>" + "<div class='clear'></div>" + "</div>"+
                "<p class='datetime' contenteditable = 'true'>" + currentHistory[i].dateTime + " not deleted and edited" + "</p>" + "<div class='clear'></div>";
                chatDisplay.innerHTML += templateDiv;
            }
        }
        lastClicked = clickedElement.substring(5, toIndex);
    });




    refresh.addEventListener('click', function () {
        var myKey = userName + lastClicked;
        var currentHistory = [];
        localStorage.setItem(myKey, JSON.stringify(currentHistory));
        document.getElementById('chatDisp').innerHTML = "";
        alert("Current chat is cleared");

        // var MyRegion = Marionette.Region.extend({
        //   el: '#content'
        // });
        //
        // var MyOtherRegion = Marionette.Region.extend();
        //
        // App.addRegions({
        //   contentRegion: MyRegion,
        //
        //   navigationRegion: '#navigation',
        //
        //   footerRegion: {
        //     el: '#footer',
        //     regionClass: MyOtherRegion
        //   }
        // });
    });


    send.addEventListener('click', function() {
        if (document.getElementById('messageInput').value == ""){
            alert("Please, Enter something in Chat....");
            return
        }

        var message = {
            name : userName,
            text : document.getElementById('messageInput').value,
            dateTime : new Date().toLocaleTimeString() + " " + new Date().toDateString(),
        };

        var allFriends = JSON.parse(localStorage.getItem("friends"));
        for(var i = 0; i < allFriends.length; i++) {
            var curName = allFriends[i].name;
            if(curName == userName) {
                allFriends[i].lastVisit = message.dateTime.substring(0, 8);
            }
        }

        localStorage.setItem("friends", JSON.stringify(allFriends));

        totalSMS++;
        var curString = lastClicked;
        var myKey = "";
        if(curString.includes('>')) {
            var endIndex = 0, startIndex;
            //alert("curString " + lastClicked);
            for(var k = 0; k < lastClicked.length; k++) {
                if(lastClicked[k] == '>') {
                    startIndex = k + 1;
                    break;
                }
            }

            for(var p = 0; p < curString.length; p++) {
                if(!isNaN(curString[p])) {
                    endIndex = p;
                    break;
                }
            }
            myKey = userName + lastClicked.substring(startIndex, endIndex);
        }
        else {
            myKey = userName + lastClicked;
        }
            console.log("SUBMITTED "  + "with " + myKey);


        var currentHistoryTo = JSON.parse(localStorage.getItem(myKey));
        currentHistoryTo.push(message);

        var templateDiv =
            "<div class='message'>" +
            "<img class='avatar' src = 'img/bkb.png'"+ currentHistoryTo[currentHistoryTo.length - 1].src +"' alt=''/>" +
            "<p class='text'>" + currentHistoryTo[currentHistoryTo.length - 1].text + "</p>" + "<div class='clear'></div>" + "</div>"+
        "<p class='datetime'>" + currentHistoryTo[currentHistoryTo.length - 1].dateTime +  " not deleted and edited" + "</p>" + "<div class='clear'></div>";

        chatDisplay.innerHTML += templateDiv;
        localStorage.setItem(myKey, JSON.stringify(currentHistoryTo));
    });


    $(document).keypress(function(e) {
        if(e.which == 13) {
            //alert('You pressed enter!');
            console.log("ENTER PRESSED");
            if (document.getElementById('messageInput').value == ""){
                alert("Please, Enter something in Chat....");
                return
            }

            var message = {
                name : userName,
                text : document.getElementById('messageInput').value,
                dateTime : new Date().toLocaleTimeString() + " " + new Date().toDateString(),
            };
            // var mgr = new Marionette.Region({
            //   el: document.querySelector("body")
            // });

            //Updating lastVisit time for the current user
            var allFriends = JSON.parse(localStorage.getItem("friends"));
            for(var i = 0; i < allFriends.length; i++) {
                var curName = allFriends[i].name;
                if(curName == userName) {
                    allFriends[i].lastVisit = message.dateTime.substring(0, 8);
                    //alert(userName);
                    //alert(allFriends[i].lastVisit);
                }
            }
            localStorage.setItem("friends", JSON.stringify(allFriends));
            totalSMS++;
            var curString = lastClicked;
            var myKey = "";
            if(curString.includes('>')) {
                var endIndex = 0, startIndex;
                //alert("curString " + lastClicked);
                for(var k = 0; k < lastClicked.length; k++) {
                    if(lastClicked[k] == '>') {
                        startIndex = k + 1;
                        break;
                    }
                }

                for(var p = 0; p < curString.length; p++) {
                    if(!isNaN(curString[p])) {
                        endIndex = p;
                        break;
                    }
                }
                myKey = userName + lastClicked.substring(startIndex, endIndex);
            }
            else {
                myKey = userName + lastClicked;
            }
                console.log("SUBMITTED "  + "with " + myKey);


            var currentHistoryTo = JSON.parse(localStorage.getItem(myKey));
            currentHistoryTo.push(message);

            var templateDiv =
                "<div class='message'>" +
                "<img class='avatar' src = 'img/bkb.png'"+ currentHistoryTo[currentHistoryTo.length - 1].src +"' alt=''/>" +
                "<p class='text'>" + currentHistoryTo[currentHistoryTo.length - 1].text + "</p>" + "<div class='clear'></div>" + "</div>"+
            "<p class='datetime'>" + currentHistoryTo[currentHistoryTo.length - 1].dateTime +  " not deleted and edited " + "</p>" + "<div class='clear'></div>";

            chatDisplay.innerHTML += templateDiv;
            localStorage.setItem(myKey, JSON.stringify(currentHistoryTo));
        }
    });
});

inputField.addEventListener('keydown', function() {
    typing.style.visibility = "visible";
});

inputField.addEventListener('keyup', function(){
    typing.style.visibility = "hidden";
});

 var displayChat = document.getElementById('chatDisp');
 if(!localStorage.getItem("chat")) {
     localStorage.setItem("chat", JSON.stringify([]));
 }
